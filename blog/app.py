import falcon

from .post import Resource

api = application = falcon.API()

post = Resource()
api.add_route('/post', post)
import falcon


class Resource(object):

    def on_get(self, req, resp):
        doc = {u'title': u'Title'}

        resp.media = doc
        resp.content_type = falcon.MEDIA_JSON
        resp.status = falcon.HTTP_200